package com.gmaillikelistview.adapter;

import com.gmaillikelistview.app.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter {

	LayoutInflater inflater;
	public ListAdapter(Context context) {
		// TODO Auto-generated constructor stub
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 8;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int itemId) {
		// TODO Auto-generated method stub
		return itemId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup container) {
		// TODO Auto-generated method stub
		if(convertView == null) {
			convertView = inflater.inflate(R.layout.list_item, null);
		
			//Put some dummy text here...
			TextView textView = (TextView) convertView.findViewById(R.id.text);
			textView.setText("Some Dummy Item Text");
		}
		
		return convertView;
	}

}
