package com.gmaillikelistview.control;

import android.R;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class ListItemView extends LinearLayout implements Checkable {

	private int[] statechecked = {R.attr.state_checked};
	private boolean checked;
	
	public ListItemView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public ListItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isChecked() {
		// TODO Auto-generated method stub
		return checked;
	}

	@Override
	public void setChecked(boolean checked) {
		// TODO Auto-generated method stub
		this.checked = checked;
		refreshDrawableState();
	}

	@Override
	public void toggle() {
		// TODO Auto-generated method stub
		this.checked = !this.checked;
		refreshDrawableState();
	}
	
	@Override
	public int[] onCreateDrawableState(int additionalState) {
		int[] newStates = super.onCreateDrawableState(additionalState + 1);
		if(checked) mergeDrawableStates(newStates, statechecked);
		
		return newStates;
	}

}
